module.exports={
    isLoggedin(req,res,next){
        if(req.isAuthenticated()){
            return next();
        }
        else{
            return res.redirect('/signin')
        }
    },
    isNotloggedIn(req,res,next){
        if(!req.isAuthenticated()){
            return next();
        }
        else{
            return res.redirect('/profile')
        }
    }
}