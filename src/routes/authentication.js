const express = require('express');
const router = express.Router();
const passport = require('passport');
const {isLoggedin, isNotloggedIn}=require('../lib/auth');

router.get('/signup',isNotloggedIn,(req,res)=>{
    res.render('auth/signup');
});
router.get('/signin',isNotloggedIn,(req,res)=>{
    res.render('auth/signin');
});
router.post('/signin',isNotloggedIn,(req,res,next)=>{
    passport.authenticate('local.signin',{
        successRedirect:'/profile',
        failureRedirect:'/signin'
    })(req,res,next)
});
router.post('/signup',isNotloggedIn,passport.authenticate('local.signup',{
    successRedirect:'/profile',
    failureRedirect:'/signup'
}));
router.get('/profile',isLoggedin,(req,res)=>{
    res.render('profile');
});
router.get('/logout',isLoggedin,(req,res)=>{
    req.logOut();
    res.redirect('/signin');
});
module.exports = router;
