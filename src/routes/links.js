const express = require('express');
const router = express.Router();
const pool = require('../database');
const {isLoggedin} = require('../lib/auth')

//Envoltorio
const insertLinks = async(newLinks)=>{
    return new Promise((resolve,reject)=>{
        const queryString = 'INSERT INTO links set ?';
        pool.query(queryString,[newLinks],(error,result,fields)=>{
            if(error) return reject(error);
            const data = {result,fields};
            return resolve(data);
        });
    });
};

router.get('/add',isLoggedin, (req,res)=>{
    res.render('links/add');
});
//obteniendo los links
router.get('/',isLoggedin,async (req,res)=>{
    const links = await pool.query('SELECT * FROM links');
    res.render('links/list',{ links });
});
//eliminando links
router.get('/delete/:id',isLoggedin, async (req,res)=>{
    const {id} = req.params;
    await pool.query('DELETE FROM links WHERE ID = ?',[id]);
    req.flash('success','link deleted');
    res.redirect('/links');
});
//edit
router.get('/edit/:id',isLoggedin, async (req,res)=>{
    const {id} = req.params;
    console.log(req.params)
    const links = await pool.query('SELECT * FROM links WHERE id = ?',[id]);
    res.render('links/edit',{link:links[0]});
});
//editado
router.post('/edit/:id',isLoggedin, async (req,res)=>{
    const {id} = req.params;
    const {tittle,url,description} = req.body;
    const newLink = {tittle,url,description};
    await pool.query('UPDATE links SET tittle=?,url=?,description=?  WHERE id = ?',[newLink.tittle,newLink.url,newLink.description, id]);
    req.flash('success','Link updated')
    res.redirect('/links');
});
//insertando links
router.post('/add',isLoggedin, async (req,res)=>{
    const{tittle, url, description} = req.body;
    const newLink = {tittle, url,description};
    try{
        const data = await insertLinks(newLink);
        req.flash('success','Link saved');

        res.redirect('/links');
        return res.status(200).send('recibido');
    } 
    catch(err){
        console.log(err);
        return res.status(500).send('error');
    }
    
});

module.exports = router;